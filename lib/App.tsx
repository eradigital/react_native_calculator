// @flow
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Provider} from 'react-redux';
import Toast from 'react-native-toast-message';
import moment from 'moment';
import MainRoutes from './presentation/routes';
import store from './application';

//helper

function App() {
  var idLocale = require('moment/locale/id');
  moment.updateLocale('id', idLocale);

  return (
    <React.Fragment>
      <Provider store={store}>
        <SafeAreaProvider>
          <NavigationContainer>
            <MainRoutes />
          </NavigationContainer>
        </SafeAreaProvider>
      </Provider>
      <Toast ref={ref => Toast.setRef(ref)} />
    </React.Fragment>
  );
}

export default App;
