import {snackbar} from '../../core/utils/snackbar.util';

export const CALCULATOR = {
  LOAD_INIT: 'CALCULATOR_LOAD_INIT',
  LOAD_SUCCESS: 'CALCULATOR_LOAD_SUCCESS',
  LOAD_FAIL: 'CALCULATOR_LOAD_FAIL',
};

function compare(a, b) {
  if (a.id < b.id) {
    return -1;
  }
  if (a.id > b.id) {
    return 1;
  }
  return 0;
}

export const sumValues = payload => {
  let dataValues = payload.filter(el => el?.active === true);
  let total = 0;
  if (dataValues.length > 1) {
    dataValues.map(item => {
      total += item.value;
    });
    return {type: CALCULATOR.LOAD_SUCCESS, total};
  } else {
    snackbar(
      'info',
      'top',
      'Harap checklist input paling tidak dua harus aktif',
    );
    return {type: CALCULATOR.LOAD_SUCCESS, total};
  }
};

export const divideValues = payload => {
  let dataValues = payload.filter(el => el?.active === true);
  let sortValues = dataValues.sort(compare);

  let total = 0;
  if (sortValues.length > 1) {
    sortValues.map((item, index) => {
      if (index === 0) {
        total += item.value;
      } else {
        total = total / item.value;
      }
    });
    let result = isFinite(total) ? total : 'Tidak Terhingga';
    return {type: CALCULATOR.LOAD_SUCCESS, total: result};
  } else {
    snackbar(
      'info',
      'top',
      'Harap checklist input paling tidak dua harus aktif',
    );
    return {type: CALCULATOR.LOAD_SUCCESS, total};
  }
};

export const minValues = payload => {
  let dataValues = payload.filter(el => el?.active === true);
  let sortValues = dataValues.sort(compare);

  let total = 0;
  if (sortValues.length > 1) {
    sortValues.map((item, index) => {
      if (index === 0) {
        total += item.value;
      } else {
        total = total - item.value;
      }
    });
    return {type: CALCULATOR.LOAD_SUCCESS, total};
  } else {
    snackbar(
      'info',
      'top',
      'Harap checklist input paling tidak dua harus aktif',
    );
    return {type: CALCULATOR.LOAD_SUCCESS, total};
  }
};

export const multipleValues = payload => {
  let dataValues = payload.filter(el => el?.active === true);
  let sortValues = dataValues.sort(compare);

  let total = 0;
  if (sortValues.length > 1) {
    sortValues.map((item, index) => {
      if (index === 0) {
        total += item.value;
      } else {
        total = total * item.value;
      }
    });
    return {type: CALCULATOR.LOAD_SUCCESS, total};
  } else {
    snackbar(
      'info',
      'top',
      'Harap checklist input paling tidak dua harus aktif',
    );
    return {type: CALCULATOR.LOAD_SUCCESS, total};
  }
};
