import {CALCULATOR} from '../../actions/calculator.action';

const initialState = {
  posts: 0,
  status: 'init',
  error: null,
};

const dataReducer = (state = initialState, action) => {
  if (action.type === CALCULATOR.LOAD_INIT) {
    const newState = {
      posts: state.posts,
      status: 'loading',
      error: null,
    };
    return newState;
  } else if (action.type === CALCULATOR.LOAD_SUCCESS) {
    const newState = {
      posts: action.total,
      status: 'succeeded',
      error: null,
    };
    return newState;
  } else if (action.type === CALCULATOR.LOAD_FAIL) {
    const newState = {
      posts: state.posts,
      status: 'failed',
      error: null,
    };
    return newState;
  }
  return state;
};

export default dataReducer;
