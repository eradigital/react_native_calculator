import {combineReducers} from 'redux';

// REDUCER
import calculator from './calculator/calculator.reducer';

const rootReducer = combineReducers({
  calculator,
});

export default rootReducer;
export type RootState = ReturnType<typeof rootReducer>;
