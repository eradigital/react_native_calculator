import React, {FunctionComponent} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import {colors} from '../../../core/constants/colors.constant';

const RoundedButtonIconComponent: FunctionComponent<{
  onPress: () => void;
  icon: string;
  type: string;
}> = props => {
  const {onPress, icon, type} = props;

  return (
    <TouchableOpacity onPress={onPress} style={styles.wrapper}>
      {type === 'AntDesign' && (
        <AntDesign name={icon} size={30} color="white" />
      )}
      {type === 'Feather' && <Feather name={icon} size={30} color="white" />}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    paddingVertical: 7,
    paddingHorizontal: 15,
    backgroundColor: colors.primary,
    borderRadius: 10,
    alignSelf: 'flex-start',
    marginHorizontal: 10,
  },
});

export default React.memo(RoundedButtonIconComponent);
