import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {colors} from '../../../core/constants/colors.constant';
import fontStyle from '../../../core/styles/font.style';

export default React.memo(function HeaderComponent() {
  return (
    <View style={styles.wrappers}>
      <Text style={fontStyle.headerOne}>Calculator</Text>
    </View>
  );
});

const styles = StyleSheet.create({
  wrappers: {
    backgroundColor: colors.primary,
    paddingVertical: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
