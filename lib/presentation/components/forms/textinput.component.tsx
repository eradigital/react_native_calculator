import React, {FunctionComponent} from 'react';
import {SafeAreaView, StyleSheet, TextInput} from 'react-native';

const TextInputComponent: FunctionComponent<{
  placeholder?: string;
  multiline?: boolean;
  keyboardType: any;
  value: string | undefined;
  onChange: (text: string) => void;
}> = props => {
  const {value, onChange, placeholder, keyboardType, multiline} = props;

  return (
    <SafeAreaView>
      <TextInput
        style={styles.input}
        onChangeText={onChange}
        value={value}
        multiline={multiline}
        placeholder={placeholder}
        keyboardType={keyboardType}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
  },
});

export default React.memo(TextInputComponent);
