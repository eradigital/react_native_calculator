import React, {FunctionComponent} from 'react';
import CheckBox from '@react-native-community/checkbox';

const CheckBoxComponent: FunctionComponent<{
  value: boolean;
  onValueChange: (value: boolean) => void;
}> = props => {
  const {value = false, onValueChange} = props;

  return <CheckBox value={value} onValueChange={onValueChange} />;
};

export default React.memo(CheckBoxComponent);
