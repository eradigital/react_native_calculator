import React, {useState, FunctionComponent} from 'react';
import {StyleSheet, View} from 'react-native';
import TextInputComponent from '../../../components/forms/textinput.component';
import CheckBoxComponent from '../../../components/forms/checkbox.component';

const InputComponent: FunctionComponent<{
  onChange: (text: number, isCheck: boolean) => void;
}> = props => {
  const {onChange} = props;
  const [isCheck, setIsCheck] = useState(false);
  const [valueInput, setValueInput] = useState('0');

  const setChangeValue = (text: string, value: boolean) => {
    if (text === '-0' || text === '0-') {
      setValueInput('-');
      onChange(0, value);
    } else {
      let number = isNaN(parseInt(text, 10)) ? 0 : parseInt(text, 10);
      setValueInput(number.toString());
      onChange(number, value);
    }
  };

  return (
    <View style={styles.wrapper}>
      <View style={styles.inputContainer}>
        <TextInputComponent
          keyboardType="numeric"
          value={valueInput}
          onChange={text => {
            setChangeValue(text, isCheck);
          }}
        />
      </View>
      <CheckBoxComponent
        value={isCheck}
        onValueChange={value => {
          setIsCheck(value);
          setChangeValue(valueInput, value);
        }}
      />
    </View>
  );
};

export default React.memo(InputComponent);

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  inputContainer: {flex: 1},
});
