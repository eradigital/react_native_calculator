import React from 'react';
import {StyleSheet, View} from 'react-native';
import HeaderComponent from '../../components/templates/header.component';
import FormSection from './sections/form.section';
import FooterSection from './sections/footer.section';

export default function HomeScreen() {
  return (
    <View style={styles.screen}>
      <HeaderComponent />
      <FormSection />
      <FooterSection />
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: 'white',
  },
});
