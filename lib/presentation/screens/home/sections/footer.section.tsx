import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import NumberFormat from 'react-number-format';
import {colors} from '../../../../core/constants/colors.constant';
import {useSelector} from 'react-redux';
import {RootState} from '../../../../application/reducers';

export default React.memo(function FooterSection() {
  const calculator = useSelector((state: RootState) => state.calculator);

  return (
    <View style={styles.wrapper}>
      <Text>Hasil</Text>
      {!isNaN(calculator.posts) && (
        <NumberFormat
          value={calculator.posts}
          displayType={'text'}
          thousandSeparator={'.'}
          decimalSeparator={','}
          prefix={''}
          renderText={value => (
            <Text
              style={{
                fontSize: 16,
                color: 'black',
                fontWeight: 'bold',
              }}>
              {value}
            </Text>
          )}
        />
      )}
      {isNaN(calculator.posts) && (
        <Text
          style={{
            fontSize: 16,
            color: 'black',
            fontWeight: 'bold',
          }}>
          {calculator.posts}
        </Text>
      )}
    </View>
  );
});

const styles = StyleSheet.create({
  wrapper: {
    paddingVertical: 13,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    borderTopColor: colors.note,
    borderTopWidth: 1,
  },
});
