import React, {FunctionComponent} from 'react';
import RoundedButtonIconComponent from '../../../components/buttons/rounded_btn_icon.component';
import {
  sumValues,
  minValues,
  multipleValues,
  divideValues,
} from '../../../../application/actions/calculator.action';
import {useDispatch} from 'react-redux';
import {StyleSheet, View} from 'react-native';

const ButtonSection: FunctionComponent<{
  dataValues: any;
}> = props => {
  const {dataValues} = props;
  const dispatch = useDispatch();

  return (
    <View style={styles.wrapperButton}>
      <View>
        <RoundedButtonIconComponent
          type="AntDesign"
          icon="minus"
          onPress={() => {
            dispatch(minValues(dataValues));
          }}
        />
      </View>
      <View>
        <RoundedButtonIconComponent
          type="AntDesign"
          icon="plus"
          onPress={() => {
            dispatch(sumValues(dataValues));
          }}
        />
      </View>
      <View>
        <RoundedButtonIconComponent
          type="AntDesign"
          icon="close"
          onPress={() => {
            dispatch(multipleValues(dataValues));
          }}
        />
      </View>
      <View>
        <RoundedButtonIconComponent
          type="Feather"
          icon="divide"
          onPress={() => {
            dispatch(divideValues(dataValues));
          }}
        />
      </View>
    </View>
  );
};

export default React.memo(ButtonSection);

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    marginTop: 20,
  },
  wrapperButton: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
});
