import React, {useState} from 'react';
import {StyleSheet, View} from 'react-native';
import InputComponent from '../components/input.component';
import ButtonSection from './button.section';

export default React.memo(function FormSection() {
  const [dataValues, setDataValues] = useState<any>([]);

  const onChangeHandler = React.useCallback(
    (id, text, isCheck) => {
      let data = {id: id, value: text, active: isCheck};
      let unLoad = dataValues.filter(el => el?.id !== id);
      setDataValues([...unLoad, data]);
    },
    [dataValues],
  );

  const memoizedValue = React.useMemo(() => dataValues, [dataValues]);

  return (
    <View style={styles.wrapper}>
      <InputComponent
        onChange={(text, isCheck) => {
          onChangeHandler(1, text, isCheck);
        }}
      />
      <InputComponent
        onChange={(text, isCheck) => {
          onChangeHandler(2, text, isCheck);
        }}
      />
      <InputComponent
        onChange={(text, isCheck) => {
          onChangeHandler(3, text, isCheck);
        }}
      />
      <ButtonSection dataValues={memoizedValue} />
    </View>
  );
});

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    marginTop: 20,
  },
});
