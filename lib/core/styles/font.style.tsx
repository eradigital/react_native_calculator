import {StyleSheet} from 'react-native';

const fontStyle = StyleSheet.create({
  headerOne: {
    fontWeight: 'bold',
    color: 'white',
    fontSize: 16,
  },
});

export default fontStyle;
