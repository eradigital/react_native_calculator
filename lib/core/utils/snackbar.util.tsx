import Toast from 'react-native-toast-message';

export const snackbar = (type, position, message) => {
  let title = '';

  if (type === 'success') {
    title += 'Success';
  }
  if (type === 'error') {
    title += 'Error';
  }
  if (type === 'info') {
    title += 'Info';
  }

  Toast.show({
    type: type, //success | error | info
    position: position, //top | bottom
    text1: title,
    text2: message,
    autoHide: false,
    visibilityTime: 5000,
    topOffset: 20,
    bottomOffset: 40,
    onShow: () => {},
    onHide: () => {},
    onPress: () => {},
  });
};
