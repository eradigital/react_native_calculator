//dark->derker->darkest | opacity one to ten
//neutral | opacity one to ten
//light->lighter->lightest | opacity one to ten

export const colors = {
  primary: '#13AAFF',
  secondary: '#1B9FFF',
  note: '#999999',
};
