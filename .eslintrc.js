module.exports = {
  root: true,
  extends: '@react-native-community',
  parserOptions: {
    ecmaVersion: 6,
    ecmaFeatures: {
      experimentalObjectRestSpread: true,
      jsx: true,
    },
    env: {
      browser: true,
      node: true,
      es6: true,
    },
  },
  plugins: ['react'],
  rules: {'react-native/no-inline-styles': 0},
};
